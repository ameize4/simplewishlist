import os

from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker

engine = create_engine(os.environ['DB_STRING'])
Session = scoped_session(sessionmaker(bind=engine))
