from sqlalchemy import Column, String, Integer
from sqlalchemy.ext.declarative import declarative_base

import wl.db_connect

Base = declarative_base()

sess = wl.db_connect.Session()


class BaseModel(Base):
    __abstract__ = True

    def save(self):
        sess.add(self)
        sess.commit()

    def delete(self):
        sess.delete(self)
        sess.commit()


class Card(BaseModel):
    __tablename__ = "card"
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    price = Column(String(50))
    link = Column(String(50))
    description = Column(String(50))

    def __init__(self, name, price, link, description):
        self.name = name
        self.price = price
        self.link = link
        self.description = description
