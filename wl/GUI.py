import sys

from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QWidget, QHBoxLayout, QPushButton, QLabel, QMainWindow, QAction, QStackedWidget, \
    QMessageBox, QTableWidget, QVBoxLayout, QInputDialog, QLineEdit, QFormLayout, QTableWidgetItem, QAbstractItemView, \
    qApp

from wl.db_requests import get_all_cards, find_card_by_id
from wl.models import Card


class Window(QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(700, 300, 700, 600)
        self.setWindowTitle("PyQt Ecuador")
        self.setWindowIcon(QIcon('pythonlogo.png'))

        # Add action to leave
        leave_action = QAction("Leave", self)
        leave_action.setShortcut("Ctrl+Q")
        leave_action.setStatusTip("Leave the hill!")
        leave_action.triggered.connect(self.close_application)
        self.statusBar()
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu("&File")
        fileMenu.addAction(leave_action)

        # Add loggin widget
        self.central_widget = QStackedWidget()
        self.setCentralWidget(self.central_widget)
        login_widget = LoginWidget(self)
        login_widget.button.clicked.connect(self.login)
        self.central_widget.addWidget(login_widget)

        self.show()

    def login(self):
        wish_screen_widget = WishScreenWidget(self)
        self.central_widget.addWidget(wish_screen_widget)
        self.central_widget.setCurrentWidget(wish_screen_widget)

    def close_application(self):
        choise = QMessageBox.question(self, "Warning", "Are you want to leave?", QMessageBox.Yes | QMessageBox.No)
        if choise == QMessageBox.Yes:
            sys.exit()
        else:
            pass


class LoginWidget(QWidget):
    def __init__(self, parent=None):
        super(LoginWidget, self).__init__(parent)
        layout = QHBoxLayout()
        self.button = QPushButton('Login')
        layout.addWidget(self.button)
        self.setLayout(layout)
        # you might want to do self.button.click.connect(self.parent().login) here


class WishScreenWidget(QWidget):
    def __init__(self, parent=None):
        super(WishScreenWidget, self).__init__(parent)
        layout = QVBoxLayout()

        self.table = QTableWidget(0, 6)

        self.update_table()
        # Lock table editing before onclick edit isn't ready
        self.table.setEditTriggers(QAbstractItemView.NoEditTriggers)
        layout.addWidget(self.table)

        self.add_card_widget = AddCardWidget()
        layout.addWidget(self.add_card_widget)

        self.setLayout(layout)

    def update_table(self):
        self.table.setRowCount(0)
        self.table.clear()
        self.table.setHorizontalHeaderLabels(['id', 'Name', 'Price', 'Link', 'Description', 'Actions'])
        cards = get_all_cards()
        for i in range(len(cards)):
            self.table.insertRow(self.table.rowCount())
            self.table.setItem(i, 0, QTableWidgetItem(str(cards[i].id)))
            self.table.setItem(i, 1, QTableWidgetItem(cards[i].name))
            self.table.setItem(i, 2, QTableWidgetItem(cards[i].price))
            self.table.setItem(i, 3, QTableWidgetItem(cards[i].link))
            self.table.setItem(i, 4, QTableWidgetItem(cards[i].description))
            self.table.setCellWidget(i, 5, EditButtonsWidget())
            self.table.resizeRowsToContents()


class AddCardWidget(QWidget):
    # TODO delete weird padding
    # TODO move to new window

    # TODO add validation for fields
    def __init__(self, parent=None):
        super(AddCardWidget, self).__init__(parent)
        e1 = QLineEdit()
        e2 = QLineEdit()
        e3 = QLineEdit()
        e4 = QLineEdit()

        flo = QFormLayout()
        flo.addRow("Name", e1)
        flo.addRow("Price", e2)
        flo.addRow("Link", e3)
        flo.addRow("Description", e4)

        self.button = QPushButton("Create card")
        flo.addWidget(self.button)
        self.button.clicked.connect(self.on_button_clicked)

        self.setLayout(flo)

    def on_button_clicked(self):
        children = self.findChildren(QLineEdit)
        content = []
        for child in children:
            content.append(child.text())
        new_card = Card(*content)
        new_card.save()
        self.parentWidget().update_table()

    def push_card(self):
        children = self.findChildren(QLineEdit)
        content = []
        for child in children:
            child.setText("")


class EditButtonsWidget(QWidget):

    def __init__(self, parent=None):
        super(EditButtonsWidget, self).__init__(parent)

        # add your buttons
        layout = QVBoxLayout()

        # adjust spacings to your needs
        layout.setContentsMargins(0, 0, 0, 0)
        layout.setSpacing(0)

        # add buttons
        button1 = QPushButton('Edit')
        button2 = QPushButton('Delete')
        button1.clicked.connect(self.edit)
        button2.clicked.connect(self.delete)
        layout.addWidget(button1)
        layout.addWidget(button2)

        self.setLayout(layout)

    def edit(self):
        table = self.parent().parent()
        row, col = self.current_position(table)
        for i in range(col):
            print(table.item(row, i).text())

        return

    def current_position(self, table):
        clickme = qApp.focusWidget()
        index = table.indexAt(clickme.parent().pos())
        if index.isValid():
            return [index.row(), index.column()]

    def delete(self):
        table = self.parent().parent()
        row, col = self.current_position(table)
        current_card = find_card_by_id(id=int(table.item(row, 0).text()))
        current_card.delete()
        table.parent().update_table()