"""create card table

Revision ID: 413a605d7a75
Revises: 
Create Date: 2018-11-28 10:42:34.879204

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '413a605d7a75'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'card',
        sa.Column('id', sa.Integer(), primary_key=True),
        sa.Column('name', sa.String(50)),
        sa.Column('price', sa.String(50)),
        sa.Column('link', sa.String(50)),
        sa.Column('description', sa.String(50))
    )


def downgrade():
    op.drop_table('card')
