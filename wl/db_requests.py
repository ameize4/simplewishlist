from wl.db_connect import Session
from wl.models import Card


def get_all_cards():
    sess = Session()
    return [*sess.query(Card).filter()]


def find_card_by_id(id):
    sess = Session()
    return sess.query(Card).filter_by(id=id).first()
